**galaxyRate by Filippo Santoliquido**

Welcome to the user guide for galaxyRate! This open-source code models with observational scaling relations the formation and host galaxies of compact objects mergers across the history of the Universe. By following this guide, you'll learn how to use galaxyRate.

**RELATED PAPERS:**

If you have any questions regarding the use of galaxyRate or need further assistance, please don't hesitate to reach out to the following contacts:

- Filippo Santoliquido: filippo.santoliquido@unipd.it
- Michela Mapelli: michela.mapelli@unipd.it

Additionally, if you use galaxyRate for your publication, we kindly request that you include the following citations:
- Santoliquido et al., 2022, MNRAS - https://ui.adsabs.harvard.edu/abs/2022MNRAS.516.3297S/abstract
This paper provides in-depth details about the code and the scientific output generated using galaxyRate. We encourage you to explore it for a comprehensive understanding of the code capabilities and insights.


**FILES**

galaxyRate is divided in three folders:
1. 1_formation_galaxies
2. 2_merger_rate_density
3. 3_host_galaxies

and other two additional folders:

4. astro_models/ for input astrophysical models (see below)
5. galaxy_models/ for output storage (see below)

**INPUT**

To effectively organize your input in galaxyRate, please follow these guidelines:

1. Create a directory named "astro_models" to store all your astrophysical models.


2. Within the "astro_models" directory, create subdirectories for different models. Each subdirectory should correspond to a specific astrophysical model and should have a descriptive name.


3. Each model subdirectory will contain the catalogs of compact object mergers in "input_catalogs", which should be further organized based on metallicity. For example, you can have files like "data_BBHs_0.02.dat" for binary black holes with a metallicity of 0.02.


An example is provided. The astrophysical model is called "A5".


**RUN and OUTPUT**

galaxyRate is entirely written in jupyter notebooks.

In 1_formation_galaxies/ you find "cosmo_model_hybrid_v2.ipynb." This jupyter notebook creates the population of star-forming galaxies, in which binary compact objects form, and the population of passive galaxies (in this version of the code, passive galaxies cannot be formation galaxies). This is the jupyter notebook in which you define all the free parameters of you star-forming galaxy poppulation. For example, you can choose the GSMF, the main sequence of galaxies, the metallicity distirbution. Several different options are provided, all of them are outlined and commented within the jupyter notebook. 
Each set of free parameters defines a galaxy model M_N. _N_ counts the number of models with different settings generated up to that point. The values of the free parameters are added and saved in the a csv file in galaxy_models/galaxy_models_list.csv. The ouput of this section are stored in the corresponding model in galaxy_models/M_N/Tables/. Several inspecting plots are also generated. 

Once the star-forming galaxy population has been created, you can evaluate the merger rate density by running the jupyter notebook "cosmo_1_rate_v3.ipynb" in 2_merger_rate_density/. The merger rate density will be saved in the corresponding galaxy models in galaxy_models/M_N where _N_ identifies a unique galaxy model. The main output of this section is a file called MRD_averaged_*option_names*.dat. This contains, 
- column 0: redshift, 
- column 1: merger rate denisy in \[Gpc^-3, yr^-1\] 

The properties of host galaxies are found by running "cosmo_3_host_assigner.ipynb" in 3_host_galaxies/. In this jupyter notebook you have to specify the merger redshift at which you want to reconstruct the host galaxies. In this version of the code, the user is provided with the conditional probability evaluated only in the local Universe from the merger trees of the EAGLE cosmological simulation. If you are interested in other merger redshift, you have to upload the corresponding merger trees and then run "cosmo_2_prob_estimator.ipynb" to evaluate the conditional probability.  The main output of this section is a file called Host_galaxies_*option_names*.h5. This is a hdf5 file which contains divided by groups the host galaxies. For each of them, a table is provided with, in the first row:

- column 0: Stellar Mass in Msun
- column 1: SFR in Msun yr^-1
- column 2: log-metallicity 
- column 3: ID (0 == passive, 1 == star-forming)
- column 4: merger redshift 
- column 5: total merger rate \[Gyr^-1\]

The first row contains the property of the host galaxy. Subsequent rows are the properties of progenitor galaxies formed
at any z_form > z_merg:

- column 0: Stellar Mass at z_form in Msun
- column 1: SFR at z_form in Msun yr^-1
- column 2: log-metallicity at z_form
- column 3: ID (1 == star-forming -> in this version all formation galaxies are star-forming)
- column 4: z_form 
- column 5: merger rate \[Gyr^-1\] at z_merg

Several different plots for inspections are also provided in the same folder. 

The user will find examples of both input and ouptut files. These are not intended to provide any scientific result by themself, since the statiscs is very low. 

** Further details **
For more detailed instructions and additional information, you can refer to the wiki of cosmoRate.